# Python 클래스의 이해: 템플릿에서 객체까지

## 클래스
> *클래스는 데이터와 함수를 함께 묶는 수단을 제공한다. 새 클래스를 만들면 객체의 새로운 타입이 생성되어 해당 타입의 인스턴스를 새로 만들 수 있다. 각 클래스 인스턴스에는 상태를 유지하기 위한 속성이 첨부될 수 있다. 클래스 인스턴스는 상태를 수정하기 위한 메서드(클래스에서 정의)도 가질 수 있다.*

이것이 Python 문서에 나오는 클래스의 정의이다. 간단히 설명해 보겠다.

클래스는 특정 모양(속성)을 가진 템플릿이라고 할 수 있으며, 해당 템플릿에서 모델을 만들고 싶을 때 속성을 가져와서 값을 지정하고 모델을 빌드할 때 객체를 빌드하면 된다.

![](./images/1_qrF19pzdeDYgLN4Eve_rcg.webp)

- 이 **템플릿**과 같은 **클래스**를 생각해 볼 수 있다. 이 템플릿에는 **제품(객체)**을 특정 모양으로 만드는 **모양(속성)**이 있다.

![](./images/1_TUJWuOG5zHTjrmkDcmvoww.webp)

- 제품을 **객체**라고 한다.

### 실제 예

![](./images/1_hUCXIQ_LDvlEohDuTVfi4A.webp)

> **Note**: 이 구문은 개념을 단순화하기 위한 것이 아니다.

## 클래스의 생성과 사용
거의 모든 것을 모델링하는 데 클래스를 사용할 수 있다. 특정 개가 아닌 일반적인 개를 표현하기 위해 Dog라는 간단한 클래스를 만들어 보자.

반려동물로 키우는 대부분의 개에 대해 우리는 무엇을 알고 있을까? 개들은 모두 **이름(name)**과 **나이(age)**가 있다. 또한 대부분의 개가 앉거나 구르는 행동을 한다는 사실도 알고 있다.

두 가지 특성(**name**과 **age**)과 두 가지 행동(**sit**과 **roll over**)이 대부분의 개에게 공통적으로 나타나는 행동이므로 `Dog` 클래스에 포함될 수 있을 것이다.

이 포스팅에서는 Python에서 개를 테마로 한 객체를 생성하는 방법을 설명하고, 클래스가 생성된 후 이를 활용해 다양한 인스턴스를 생성할 것이다. 각 인스턴스는 특정 개를 나타낸다.

**`Dog` 클래스에서 생성된 각 인스턴스는 `name`과 `age`를 저장하고, 각 개에게 `sit()`과 `roll_over()` 기능을 부여한다.**

```python
class Dog():
  """A simple attempt to model a dog."""
  def __init__(self, name, age):
    """Initialize name and age attributes."""
    self.name = name
    self.age = age

  def sit(self):
  """Simulate a dog sitting in response to a command."""
    print(self.name.title() + " is now sitting.")

  def roll_over(self):
  """Simulate rolling over in response to a command."""
    print(self.name.title() + " rolled over!")
```

- `Dog` 클래스를 생성한다. Python 클래스 이름은 전통적으로 대문자로 시작한다.
- 이 클래스는 처음부터 만들었으므로 클래스 선언의 괄호는 비어 있다.
- 이 클래스가 수행하는 작업을 설명하는 문서 문자열을 생성한다.

### `__init__()` 메소드

- 함수가 있고 이 함수가 클래스의 일부인 경우 이를 메서드라고 부른다. 
- 새로운 `Dog` 클래스의 인스턴스가 생성될 때마다 초기화 메소드를 사용한다. Python의 일반 메서드 이름과의 충돌을 피하기 위해 두 개의 시작 밑줄과 두 개의 끝 밑줄로 이 함수를 표시한다. 
- `__init__()` 메서드의 세 가지 인수를 `self`, `name`과 `age`로 지정한다.
- `self` 매개변수는 반드시 필요하므로 메서드 선언에서 다른 매개변수보다 먼저 나와야 한다.
- 나중에 (`Dog`의 인스턴스를 생성하기 위해) 이 `__init__()` 함수를 사용할 때 Python이 자동으로 `self` 인수를 제공하기 때문에 정의에서 이를 언급할 필요가 있다.
- 인스턴스 자체에 대한 참조인 `self`는 클래스에 연결된 모든 메서드 호출에 의해 자동으로 전송되어 특정 인스턴스에 클래스의 속성과 메서드에 대한 액세스 권한을 부여한다.
- 간단히 말해, `self`를 스포츠 클럽의 카드라고 생각할 수 있다. 카드가 있으면 클럽의 모든 것(속성, 메서드)에 액세스할 수 있지만 카드가 없으면 이를 할 수 없다.

![](./images/1_3K0dhBGH4hIleoKjs8xsbQ.webp)

- 설계도를 활용하여 실제 dog를 만들 때 dog에게 name과 age를 지정한다.
- Python에서는 생성한 클래스 내부에 메서드라고 하는 고유한 함수를 구축한다. 이러한 메서드 중 하나를 `__init__()`라고 하며, 새 dog를 키우기 위한 일련의 지침과 매우 유사하게 작동한다.
- `__init__()` 메서드의 지침에는 `self`, `name`과 `age`라는 세 가지 특정 항목이 언급되어 있다. "이 개" 또는 "우리가 만들고 있는 개"는 `"self"`라는 고유한 단어의 의미이다. Python은 청사진을 사용하여 dog를 만들 때 자동으로 이를 제공한다.
- 여기서 `self.name = name`과 `self.age = age`는 내 `dog` 이름은 변수 `name` 안에 있는 값과 같고, 내 dog 나이는 변수 `age` 안에 있는 값과 같다고 생각하면 된다.
- `__init__()` 함수에 대한 지침에는 `self`, `name`과 `age`의 세 가지 특정 항목이 포함되어 있다. "`self`"라는 단수 단어에는 두 가지 의미가 있다. "이dog" 또는 "우리가 만들고 있는 dog"이다. 우리가 청사진을 이용해 `개`를 만들면 Python은 자동으로 dog를 제공한다.
- 추가로 선언된 두 개의 메서드인 `sit()`과 `roll_over()`는 `name`이나 `age`와 같은 추가 데이터가 필요하지 않더라도 `self`라는 매개변수를 포함하도록 정의되어 있다.
- 이 메서드들은 나중에 생성하는 인스턴스에서 사용할 수 있다. 인스턴스는 몸을 뒤집고 앉을 수 있게 될 것이다. `roll_over()`와 `sit()`은 지금 당장 아무 일도 하지 않는다.
- 인쇄되는 메시지에서 dog는 단순히 앉거나 뒤집힌 것으로 묘사된다. 하지만 이 아이디어는 더 실제적인 시나리오에 적용될 수 있다. 이 클래스가 실제 컴퓨터 게임의 컴포넌트라면 이 메서드에는 애니메이션 개가 앉거나 뒤집히는 코드를 포함할 것이다.
- 이 클래스가 로봇을 제어하도록 설계되었다면 이 메서드들은 강아지 로봇이 앉고 뒤집는 동작을 제어할 것이다.

### 클래스에서 인스턴스 만들기
클래스를 인스턴스 생성을 위한 지침 모음으로 생각해 보자. Python은 Dog 클래스로 특정 개를 나타내는 고유한 인스턴스를 생성하는 방법을 안내한다. 특정 dog를 나타내는 예를 만들어 보겠다.

```python
my_dog = Dog('willie', 6)

print("My dog's name is " + my_dog.name.title() + ".")
w print("My dog is " + str(my_dog.age) + " years old.")
```

Python에 "Willie"라는 이름의 6살짜리 개를 만들도록 지시한다. Python은 이 문장 다음에 "Willie"와 "6"을 입력으로 하여 `Dog`에서 `__init__()` 함수를 실행한다.

`name`과 `age` 속성은 `__init__()` 함수에 제공한 정보를 사용하여 설정되며, 이 함수는 이 특정 dog를 나타내는 인스턴스도 생성한다.

Python은 `__init__()` 함수에 반환 문이 명시적으로 없는 경우에도 이 dog를 나타내는 인스턴스를 자동으로 생성한다.

해당 인스턴스는 `my_dog` 변수에 보관된다. 일반적으로 `Dog`와 같이 대문자로 시작되는 이름은 클래스를 가리키고, `my_dog`와 같이 소문자로 시작하는 이름은 클래스로부터 만들어진 단일 인스턴스를 일컫도록 하는 것이 일반적인 Python에서의 명명 규칙(naming convention)이다. 바로 이 지점에서 명명 규칙이 유용하다.

### 속성 액세스
인스턴스의 속성을 액세스하려면 점 표기법을 사용한다. 다음과 같이 우리는 `my_dog`의 속성 이름 값을 액세스할 수 있다.

```python
my_dog.name
```

`my_dog`의 `name` 속성 값인 `willie`는 `my_dog.name.title()` 함수를 사용하여 대문자로 시작하도록 만들었다.

`my_dog`의 `age` 속성 값인 `6`은 두 번째 인쇄 문에서 `str(my_dog.age)` 함수를 사용하여 문자열로 변환된다.

**결과는 `my_dog`에 대한 정보에 대한 개요이다.**

![](./images/1_T86pB7T37xLJfC5cs2qBlw.webp)

### 메소드 호출
`Dog` 클래스에 지정된 모든 메서드는 클래스 인스턴스가 생성된 후 점 표기법을 사용하여 호출할 수 있다. dog가 구르고 앉도록 만들어 보자.

```python
class Dog():
   --code--

my_dog.sit() #object.method
my_dog.roll_over() #object.method
```

메서드를 호출할 때는 메서드 이름과 인스턴스 이름(이 경우 `my_dog`)을 점으로 구분한다.

Python은 `Dog` 클래스에서 `sit()` 함수를 검색하고 `my_dog.sit()`을 읽어 코드를 실행한다.

`my_dog.roll_over()`는 Python에서 같은 의미를 갖는다. 이제 `Willie`는 우리의 지시를 따른다.

![](./images/1_ENlrdYreCT0crMg32YcyMA.webp)

### 여러 인스턴스 생성하기
한 클래스에서 필요한 만큼의 인스턴스를 만들 수 있다. 두 번째 dog인 `your_dog`를 만들어 보자.

```python
my_dog = Dog('willie', 6)
your_dog = Dog('lucy', 3)
print("My dog's name is " + my_dog.name.title() + ".")
print("My dog is " + str(my_dog.age) + " years old.")
my_dog.sit()
print("\nYour dog's name is " + your_dog.name.title() + ".")
print("Your dog is " + str(your_dog.age) + " years old.")
your_dog.sit()
```

이 예에서 dog Willie와 Lucy를 만들었다. dog 각각은 고유한 특성 집합과 동일한 동작 집합을 사용할 수 있는 별개의 인스턴스이다.

![](./images/1_pLu_zr76d0dAXmEAh8ye7A.webp)

Python은 두 번째 dog에게 동일한 name과 age를 부여하더라도 여전히 Dog 클래스의 고유한 인스턴스를 생성한다. 클래스의 각 인스턴스에 고유한 변수 이름을 지정하거나 목록이나 사전에서 위치를 지정하기만 하면 해당 클래스의 인스턴스를 필요한 만큼 생성할 수 있다.

## `Car` 클래스
자동차를 나타내는 새로운 클래스를 만든다. 이 클래스는 우리가 다루는 자동차 타입에 관한 정보를 추적하고 이를 압축하는 함수를 포함할 것이다.

```python
class Car():
    """A simple attempt to represent a car."""
    def __init__(self, make, model, year)
    """Initialize attributes to describe a car"""
    self.make = make
    self. year = year
    self. model= model

    #Method from the class
    def get_descriptive_name(self):
    """Return a neatly formatted descriptive name."""
    long_name = self.year + ' ' + slef.make + ' ' + self.model
    return long_name.title()

#Creating object
my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descriptive_name())
```

`Dog` 클래스에서 했던 것과 마찬가지로 `Car` 클래스에 `self` 인수를 사용하여 `__init__()` 함수를 먼저 정의한다. `make`, `model`과 `year`라는 세 가지 요소도 추가로 제공된다. 

함수 `__init__()`는 이러한 입력을 받아 이 클래스에서 생성된 인스턴스에 연결된 속성에 저장한다. 새 자동차 인스턴스를 만들 때 `make`, `model`과 `year`를 선택해야 한다.

`year`, `make`와 `model`을 하나의 문자열로 결합하여 자동차를 간결하게 설명하는 `get_descriptive_name()`이라는 함수를 작성한다. 이렇게 하면 각 속성의 값을 개별적으로 인쇄할 필요가 없다. 이 함수에서는 `self.make`, `self.model`과 `self.year`를 활용하여 속성 값을 조작한다.

`Car` 클래스 객체를 생성하고 이를 "my_new_car" 변수에 저장한다. 다음으로 `get_descriptive_name()`을 호출하여 현재 운전 중인 차량의 타입을 표시한다.

클래스를 더 흥미롭게 만들기 위해 시간에 따라 변하는 속성을 추가해 보겠다. 자동차의 전체 주행 거리를 저장하는 속성을 추가하겠다.

### 속성의 기본값 설정
클래스에서 속성의 초기값이 0이거나 빈 문자열인 경우에도 값이 있어야 한다.

`__init__()` 메서드 본문에서 속성의 초기 값을 제공하면 기본값을 설정할 때같은 일부 상황에서는 해당 속성에 대한 매개 변수를 추가할 필요가 없다.

항상 0의 값을 갖는 `odometer_reading`이라는 속성을 추가하겠다. 각 차량의 주행 거리계를 읽을 수 있도록 `read_odometer()`라는 메서드를 추가한다.

그 전에는 초기화 메서드에서 이 속성을 `__init__()` 메서드의 () 사이에 넣지 않고 초기화 메서드 본문에 값과 함께 정의하면 속성을 사용하거나 추가할 수 있다.

```python
class Car():
    def __init__(self, make, model, year): #you didn't write it here
    """Initialize attributes to describe a car."""
    self.make = make
    self.model = model
    self.year = year
    self.odometer_reading = 0 #but used it here with value

    def get_descriptive_name(self):
    --snip--

    def read_odometer(self):
    """Print a statement showing the car's mileage."""
    print("This car has " + str(self.odometer_reading) + " miles on it.")

    my_new_car = Car('audi', 'a4', 2016)
    print(my_new_car.get_descriptive_name())
    my_new_car.read_odometer()
```

이제 이전 예에서와 같이 Python이 `__init__()` 함수를 호출하여 새 인스턴스를 생성할 때 `make`, `model` 및 `year` 값이 속성으로 저장된다. 그 후 Python은 `odometer_reading`이라는 새 속성을 생성하고 이를 `0`으로 초기화한다.

또한 `read_odometer()`라는 새 함수를 추가하여 자동차의 주행 거리를 간단하게 읽을 수 있다. 우리 자동차의 시작 주행거리는 `0`마일이다.

![](./images/1__6hl1EqXHVV_-Hi8eotefw.webp)

### 속성 값 업데이트
속성 값을 변경하는 방법에는 인스턴스를 통해 직접 변경하거나, 메서드를 사용하여 값을 수정하거나, 값을 늘릴(특정 값을 더할) 수 있다.

#### 1. 속성 값을 직접 수정
인스턴스를 통해 속성을 직접 액세스하여 값을 변경하는 것이 가장 쉬운 방법다. 여기서는 마일리지를 즉시 `23`으로 설정한다.

```python
my_new_car.odometer_reading = 23
my_new_car.read_odometer()
```

점 표기법을 사용하여 자동차의 `odometer_reading` 속성을 액세스하고 직접 값을 설정한다. Python은 이 문장을 통해 `my_new_car` 인스턴스에 연결된 속성을 찾아서 그 값을 `23`으로 설정하도록 지시받는다.

![](./images/1_plV9lcpzK8f_ugAGbw-zAA.webp)

값을 업데이트하는 메서드를 빌드하고 싶을 때도 있지만, 이렇게 직접 액세스 속성을 업데이트하도록 작성하고 싶을 때도 있다.

#### 2. 메서드를 통한 속성 값 업데이트

특정 속성을 변경하는 메서드를 사용하면 유용할 수 있다. 속성에 직접 액세스하지 않고 내부적으로 업데이트를 처리하는 메서드에 업데이트할 값을 전달하면 된다. 다음은 `update_odometer()` 함수가 실제로 작동하는 코드이다.

```python
def update_mileage(self, mileage):
  """Set the odometer reading to the given value """
  self.mileage = mileage

my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descriptive_name())

my_new_car.update_odometer(23)
my_new_car.read_odometer()
```

`update_odometer()`는 `Car`에 적용된 유일한 개선 사항이다. 이 메서드는 `self.odometer_reading` 변수에서 주행 거리 값을 읽는다.

`update_odometer()`는 메서드 설명에서 마일리지 매개변수에 `23`을 입력하여 호출하면, 주행 거리계 수치를 23으로 설정한다. `read_odometer()`는 주행 거리계 수치를 출력한다.

![](./images/1_VyHhlb1VX8Sjh7zU3sXaUw.webp)

주행 거리계 수치가 변경될 때마다 `update_odometer()` 함수를 개선하여 더 많은 작업을 수행할 수 있다. 아무도 주행 거리계 수치를 다시 변경하려고 시도하지 않도록 약간의 로직을 추가해 보겠다.

```python
class Car():
  --snip--

  def update_odometer(self, mileage):
    """
    Set the odometer reading to the given value.
    Reject the change if it attempts to roll the odometer back.
    """
    if mileage >= self.odometer_reading:
      self.odometer_reading = mileage
    else:
      print("You can't roll back an odometer!")
```

주행 거리계에 설정할 새 주행 거리를 나타내는 마일리지 인수는 `update_odometer()` 함수에서 필요하다.

이 메서드는 새 주행거리(마일리지)가 이전 주행거리(`self.odometer_reading`)를 크거나 같은지 여부를 결정한다.

새 주행거리가 이전 주행거리보다 크거나 같으면 주행거리계가 적절하게 업데이트된 것이다. 이 경우 메서드는 `self.odometer_reading = mileage`를 수행하여 주행 거리계 수치를 현재 마일로 업데이트한다.

새 거리가 현재 수치보다 작도록 주행 거리계를 재설정하려고 시도한다면, 이 접근 방식은 메시지 `You can't roll back an odometer!`를 표시하여 주행 거리계 롤백이 금지되었음을 알린다.

#### 3. 메서드를 통해 속성 값 증가

속성의 값을 새로 설정하는 대신 특정 거리만큼 값을 늘리고 싶을 수도 있다. 중고 자동차를 구입하여 등록하기 전에 100마일을 주행했다고 가정해 보겠다. 다음은 주행 거리계 판독값에 해당 값을 추가하고 이 증가량을 전달할 수 있는 기술이다.

```python
def increament_dodmeter(self, miles):
  """Add the given amount to the odometer reading """
  self.odometer_reading += miles

my_used_car = Car('subaru', 'outback', 2013)
print(my_used_car.get_descriptive_name())
my_used_car.update_odometer(23500)
my_used_car.read_odometer()
my_used_car.increment_odometer(100)
my_used_car.read_odometer()
```

새 함수 `increment_odometer()`에 주행 거리(마일)를 입력한 다음 이 값을 `self.odometer_reading`에 더합니다.

`my_used_car`라는 중고 자동차를 선언한다. `update_odometer()`를 사용하여 `23500`을 전달하여 주행 거리계를 `23,500`으로 설정한다.

자동차를 구입한 시점과 등록한 시점 사이의 `100`마일을 더하려면 `increment_odometer()`를 사용하여 `100`이라는 값을 지정한다.

![](./images/1_at_llf5x4xlTsIap3XqMLA.webp)

## 상속
클래스를 작성할 때 반드시 처음부터 시작할 필요는 없다. 작성 중인 클래스가 이미 작성된 다른 클래스의 커스터마이즈 버전인 경우 상속을 활용할 수 있다.

한 클래스가 다른 클래스를 상속하면 첫 번째 클래스의 모든 특성과 메서드를 즉시 획득할 수 있다.

새 클래스와 이전 클래스를 모두 자식 클래스라고 한다.

자식 클래스는 부모 클래스의 모든 특성과 메서드를 상속받을 뿐만 아니라 추가 특성과 메서드를 자유롭게 정의할 수 있다.

### 자식 클래스의 ` __init__()` 메소드
전기 자동차를 예로 모델링해 보겠다. 전기 자동차는 여러 종류의 자동차 중 하나에 불과하므로 앞서 작성한 자동차 클래스의 하위 클래스로 새로운 ElectricCar 클래스를 만들 수 있다.

그런 다음 전기 자동차 고유의 특성과 동작만 코딩하면 된다. 먼저 `Car` 클래스의 모든 기능을 수행하는 기본 `ElectricCar` 클래스를 만들어 보겠다.

```python
class Car():
    """A simple attempt to represent a car."""
    def __init__(self, make, model, year):
    self.make = make
    self.model = model
    self.year = year
    self.odometer_reading = 0

    def get_descriptive_name(self):
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def read_odometer(self):
        print("This car has " + str(self.odometer_reading) + " miles on it.")

    def update_odometer(self, mileage):
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else:
        print("You can't roll back an odometer!")

    def increment_odometer(self, miles):
        self.odometer_reading += miles

#Our Subclass
class ElectricCar(Car):  #it takes the parent class as a param
    """Represent aspects of a car, specific to electric vehicles. """
    def __init__(self, make, model, year, ):
    """Initialize attributes of the parent class """
    #translation: from parent(super) init function bring (make, model, year)
    super().__init__(model, year, make)

my_tesla = ElectricCar('tesla', 'model s', 2016)
print(my_tesla.get_descriptive_name())
```

`Car`에서 시작한다. 부모 클래스는 현재 파일에 존재해야 하며 자식 클래스를 생성할 때 자식 클래스 앞에 있어야 한다.

자식 클래스 `ElectricCar` 를 정의하겠다. 자식 클래스의 정의에서 부모 클래스의 이름은 괄호 안에 포함되어야 한다.

`Car` 인스턴스를 생성하는 데 필요한 데이터는 car 객체의 `__init__()` 함수로 전송된다.

`super()`는 Python에서 부모 클래스와 자식 클래스 간의 연결을 용이하게 하는 고유한 메서드이다.

이 문장은 Python이 부모 클래스의 `__init__()` 함수를 호출하여 부모 클래스의 모든 특성을 가진 `ElectricCar`의 인스턴스를 제공하도록 지시한다. `super`라는 이름은 부모 클래스를 **superclass**로, 자식 클래스를 서브클래스로 부르는 관습에서 유래했다.

기존 자동차를 만드는 데 필요한 것과 동일한 타입의 정보를 사용하여 상속이 얼마나 잘 작동하는지 알아보기 위해 전기 자동차를 만들어 보겠다.

새로운 `ElectricCar` 클래스 인스턴스를 생성하고 `my_tesla`에 저장한다. 이 문은 Python이 자식 클래스인 `ElectricCar`에 정의된 `__init__()` 메서드를 호출하여 부모 클래스인 `Car`의 `__init__()` 메서드를 실행하도록 지시한다.

`Tesla`, `Model S`, `2016`이 그 증거이다. 현재 `__init__()` 외에 전기 자동차에 특화된 속성이나 메서드는 없다.

현재로서는 전기 자동차가 적절한 자동차 동작을 보이도록 하는 것뿐이다.

![](./images/1_SL6_C6OVPb8OrSwIh6sR6w.webp)

### 자식 클래스에서 속성과 메소드 정의
자식 클래스가 부모 클래스를 상속하면 자식 클래스를 부모 클래스와 차별화하는 데 필요한 모든 추가 속성과 메서드를 추가할 수 있다.

전기 자동차에 고유한 배터리 관련 속성과 이를 보고하는 방법을 추가해 보겠다. 배터리에 대한 설명을 출력하고 배터리 크기를 저장하는 메서드를 선언하겠다.

```python
class Car():
--snip--

class ElectricCar(Car):
    """Represent aspects of a car, specific to electric vehicles."""
    def __init__(self, make, model, year):
    super().__init__(make, model, year)
    self.battery_size = 70

    def describe_battery(self):
    """Print a statement describing the battery size."""
    print("This car has a " + str(self.battery_size) + "-kWh battery.")

my_tesla = ElectricCar('tesla', 'model s', 2016)
print(my_tesla.get_descriptive_name())
my_tesla.describe_battery()
```

- `self.battery_size`라는 새 속성을 생성하고 `70`으로 초기화한다.
- 이 속성은 `Car`의 어떤 인스턴스와도 연결되지 않지만 생성되는 `ElectricCar` 클래스의 모든 인스턴스에 적용된다.
- 또한 배터리에 대한 세부 정보를 출력하는 `describe_battery()`라는 함수가 있다. 이 함수를 호출하면 전기 자동차에 특화된 설명을 얻을 수 있다.

![](./images/1_FkvlzCp1REX6gtKL8-ZUNg.webp)

- `ElectricCar` 클래스에서 전문화(specialize)할 수 있는 정도는 제한이 없다.
- 원하는 수준의 정밀도로 전기 자동차를 모델링하려면 원하는 만큼의 특성과 기술을 포함할 수 있다.
- eletric 클래스 175 차에만 적용되는 특성이나 기능을 `ElectricCar` 클래스에 추가하는 대신 `Car` 클래스를 도입해야 한다. 이 경우 `Car `클래스를 사용하는 모든 사람이 해당 기능에 액세스할 수 있으며, `ElectricCar` 클래스에는 전기 자동차 전용 데이터와 기능에 대한 코드만 포함될 것이다.

### 부모 클래스의 메서드 재정의
자식 클래스가 에뮬레이트하려는 것에 적합하지 않은 부모 클래스 메서드는 재정의할 수 있다.

이렇게 하려면 자식 클래스에서 재정의하려는 부모 클래스 메서드와 같은 이름의 메서드를 선언하면 된다.

Python은 부모 클래스 메서드를 무시하고 사용자가 작성한 자식 클래스 메서드에만 집중한다.

`Car` 클래스에 `fill_gas_tank()`라는 이름의 함수가 있다고 가정해 보자. 이 함수는 순수 전기 자동차에는 쓸모가 없으므로 재정의하고 싶을 수 있다. 이를 위한 한 가지 접근 방식이 있다.

```python
def ElectricCar(Car):
--snip--

    def fill_gas_tank():
    """Electric cars don't have gas tanks."""
    print("This car doesn't need a gas tank!")
```

- 이제 누군가가 전기 자동차에서 `fill_gas_tank()`를 호출하려고 하면 Python은 `Car`의 `fill_gas_tank()` 메서드를 무시하고 대신 이 코드를 실행한다.
- 상속을 활용하면 자식 클래스가 필요하지 않은 부모 클래스 기능을 강제로 재정의하고 원하는 기능은 유지할 수 있다.

### 속성으로 인스탄스
코드에서 실제 세계의 모든 것을 모방하려고 클래스에 점점 더 많은 정보를 추가하고 있다는 것을 알 수 있다.

파일 길이가 점점 길어지고 메서드와 속성이 늘어나는 것을 알 수 있다.

이러한 상황에서는 한 클래스의 일부를 별개의 클래스로 작성할 수 있다는 것을 깨달을 수 있다.

큰 클래스를 여러 개의 작은 클래스로 나눌 수도 있다. 예를 들어 `ElectricCar` 클래스에 정보를 계속 추가하다 보면 자동차의 배터리와 관련된 특성과 메서드가 많이 추가되는 것을 관찰할 수 있다.

이런 일이 발생하면 프로세스를 중단하고 이러한 특성과 메서드를 다른 클래스인 `Battery`로 옮길 수 있다.

그러면 `ElectricCar` 클래스의 속성으로 "`Battery`" 인스턴스를 사용할 수 있다.

```python
class Car():
--snip--

class Battery():
    """A simple attempt to model a battery for an electric car."""
    def __init__(self, battery_size=70):
    self.battery_size = battery_size

    def describe_battery(self):
    """Print a statement describing the battery size."""
    print("This car has a " + str(self.battery_size) + "-kWh battery.")

class ElectricCar(Car):
"""Represent aspects of a car, specific to electric vehicles."""
    def __init__(self, make, model, year):
    """
    Initialize attributes of the parent class.
    Then initialize attributes specific to an electric car.
    """
    super().__init__(make, model, year)
    self.battery = Battery()
    

my_tesla = ElectricCar('tesla', 'model s', 2016)
print(my_tesla.get_descriptive_name())
my_tesla.battery.describe_battery()
```

- 다른 클래스에서 파생되지 않는 위치에 `Battery`라는 이름의 새로운 클래스를 생성한다.
- `battery_size`는 `__init__()` 함수에서 `self`에 대한 추가 인수이다. 이 선택적 매개 변수에 값을 지정하지 않으면 `battery_size`가 `70`으로 설정된다.
- 또한 `describe_battery()` 함수가 이 클래스로 재배치되었다. 이제 `ElectricCar` 클래스에 `self.battery` 속성이 포함된다.
- 이 문은 Python이 새 배터리 인스턴스를 생성하고 값이 없기 때문에 기본 크기가 `70`인 `self.battery` 속성에 저장하도록 지시한다.
- 이제 `__init__()` 함수가 호출될 때마다 모든 `ElectricCar` 인스턴스에 대해 `Battery` 인스턴스가 자동으로 생성된다.

```python
my_tesla.battery.describe_battery()
```

- 이 문장은 Python이 `my_tesla` 인스턴스에서 `battery` 속성을 검색하고 `describe_battery()` 함수를 실행한 다음 속성에 저장된 배터리 인스턴스를 식별하도록 지시한다.
- 결과는 앞서 관찰한 것과 동일하다.

```
2016 Tesla Model S
This car has a 70-kWh battery.
```

- 많은 노력이 필요해 보이지만 이제 `ElectricCar` 클래스를 복잡하게 만들지 않고도 배터리를 원하는 만큼 자세히 설명할 수 있다.
- `Battery`에 `battery_size`에 따라 자동차의 주행 가능 거리를 계산하는 함수를 하나 더 추가해 보겠다.

```python
class Car():
    --snip--

class Battery():
    --snip--
    def get_range(self):
        if self.battery_size == 70:
            range = 240
        elif self.battery_size == 85:
            range = 270

        message = "This car can go approximately " + str(range)
        message += " miles on a full charge."
        print(message)

class ElectricCar(Car):
    --snip--

my_tesla = ElectricCar('tesla', 'model s', 2016)
print(my_tesla.get_descriptive_name())
my_tesla.battery.describe_battery()
my_tesla.battery.get_range()
```

- 새로운 함수 `get_range()`는 빠른 검사를 수행한다. `get_range()`는 용량이 70kWh인 배터리의 경우 주행 가능 거리를 240마일로 설정하고, 85kWh인 배터리의 경우 주행 가능 거리를 270마일로 설정한다.
- 그런 다음 이 값을 출력한다. 이 기법을 사용할 때는 차량의 배터리 특성을 사용하여 다시 한 번 호출해야 한다.
- `battery_size`에 따라 출력은 차량의 주행 가능 거리를 나타낸다.

```
2016 Tesla Model S
This car has a 70-kWh battery.
This car can go aproximately 240 miles on a full charge.
```

### 다형성(Polymorphism)
개, 고양이, 새 등 다양한 시끄러운 동물을 키운다고 생각해 보자. 그럼에도 불구하고 각 동물은 고유한 소리를 낸다. 다형성은 이 개념과 관련이 있다.

다형성이란 객체(클래스의 인스턴스)가 공통 인터페이스나 베이스 클래스를 공유하면서 다른 형태나 동작을 가질 수 있는 능력을 말한다. 간단히 말해서, 다형성은 동일한 메서드나 함수 호출에 대해 서로 다른 객체가 다르게 응답하거나 동작할 수 있다는 것을 의미한다.

**간단한 방법으로**

1. 메서드 이름을 공유하면서도 다양한 클래스의 객체는 각각 고유한 동작을 가질 수 있다.
1. 이러한 클래스는 상속을 통해 연결되는 경우가 많은데, 기본 클래스는 파생 클래스가 고유한 방식으로 사용할 수 있는 표준 인터페이스(메서드)를 설정한다.
1. 객체에서 메서드를 호출하면 객체의 클래스가 메서드가 수행되는 방식을 제어한다. 따라서 함수의 이름이 같더라도 클래스에 따라 다양한 객체에서 함수를 다르게 구현할 수 있다.
1. 따라서 다양한 객체가 서로 다르게 동작하더라도 일관되게 처리하는 코드를 설계할 수 있다.

```python
class Animal:
    def make_sound(self):
        pass

class Dog(Animal):
    def make_sound(self):
        print("Woof!")

class Cat(Animal):
    def make_sound(self):
        print("Meow!")

class Bird(Animal):
    def make_sound(self):
        print("Chirp!")

# Creating instances of different animals
dog = Dog()
cat = Cat()
bird = Bird()

# Calling the common method on different objects
dog.make_sound()  # Output: Woof!
cat.make_sound()  # Output: Meow!
bird.make_sound()  # Output: Chirp!
```

**출력**

```
Woof!
Meow!
Chirp!
```

- 이 예에서 `Dog`, `Cat` 및 `Bird`는 기본 클래스 `Animal`에서 파생된 클래스이다. 이러한 각 클래스에는 고유한 `make_sound()` 함수 구현이 있다.
- 이러한 클래스 중 하나의 인스턴스에서 생성되어 `make_sound()` 함수를 호출하는 모든 객체는 해당 객체에 대해 선택된 구현에 따라 호출된다. 모두 동일한 Animal 클래스 제공 인터페이스를 사용하지만 개는 짖고, 고양이는 야옹거리고, 새는 지저귀는 소리가 다르다.
- 이처럼 클래스의 다형성은 여러 객체가 동일한 메서드 이름에 대해 각자의 특별한 방식으로 반응할 수 있게 하여 코드의 유연성과 재사용성을 높힌다. 동작이나 모양이 서로 다르더라도 다양한 작업을 일관되게 처리할 수 있다.

## 마치며
요약하자면, Python 클래스는 정보와 기능을 수정하고 재사용할 수 있는 템플릿으로 배열하는 메커니즘을 제공한다. 클래스의 도움으로 각각 고유한 속성과 함수 집합을 가진 클래스의 인스턴스를 여러 개 만들 수 있다. 클래스는 코드 재사용과 모듈화를 촉진하는 동시에 실제 사물을 모델링하는 데 도움을 준다. 개발자는 클래스의 개념을 이해함으로써 객체 지향 프로그래밍의 장점을 활용하고 보다 체계적이고 효율적인 코드를 작성할 수 있다.

다음으로 [이 포스팅](https://pythonnecosystems.gitlab.io/solid-principles/)을 일기를 권합니다.
