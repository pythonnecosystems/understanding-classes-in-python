# Python 클래스의 이해: 템플릿에서 객체까지  <sup>[1](#footnote_1)</sup>

이 포스팅에서는 Python 클래스 개념를 소개한다. 타입의 새로운 인스턴스를 생성하기 위해 데이터와 함수를 함께 그룹화하는 템플릿으로 클래스를 정의할 수 있다. 클래스의 각 인스턴스에는 상태를 변경하는 메서드와 상태를 보존하는 속성이 포함될 수 있다. 이 포스팅에서 클래스는 속성(property)은 객체의 정확한 구조를 지정하고 객체가 생성될 때 그 형태를 취하는 템플릿 또는 청사진과 비교할 수 있다. 이 포스팅에서는 Python 클래스의 개념을 설명하기 위해 Dog 클래스의 예를 사용한다.

<a name="footnote_1">1</a>: 이 페이지는 [Understanding Classes in Python: From Templates to Objects](https://medium.com/@noransaber685/understanding-classes-in-python-from-templates-to-objects-8fa920aad3a1)을 편역한 것이다.
